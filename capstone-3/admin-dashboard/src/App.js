import * as React from "react";
import { Admin, fetchUtils, Resource, ListGuesser, Layout } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';
import { stringify } from 'query-string';
import { UserList } from "./components/UserList";
import { PostShow } from "./components/PostShow";
import { PostList } from "./components/PostList";
import { PostEdit } from "./components/PostEdit";
import { GetUserDetails } from "./components/GetUserDetails";
import PostIcon from '@mui/icons-material/Book';
import UserIcon from '@mui/icons-material/People';
import AlbumIcon from '@mui/icons-material/Book';
import CustomAppBar from "./components/CustomAppBar";

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');
// const apiUrl = jsonServerProvider(process.env.PORT || 3000);
const apiUrl = jsonServerProvider('http://localhost:3000');

// Integrating the Express app for CRUD operations

// const httpClient = (url, options = {}) => {
//   if (!options.headers) {
//     options.headers = new Headers({ Accept: 'application/json' });
//   }
//   // You can add additional headers here if needed
//   const token = localStorage.getItem('token');
//   options.headers.set('Authorization', `Bearer ${token}`);
//   return fetchUtils.fetchJson(url, options);
// };

// const dataProvider = {
//   getList: (resource, params) => {
//     const url = `${apiUrl}/${resource}`;
//     const query = {
//       ...params.filter,
//       _sort: params.sort.field,
//       _order: params.sort.order,
//       _start: (params.pagination.page - 1) * params.pagination.perPage,
//       _end: params.pagination.page * params.pagination.perPage,
//     };
//     const queryStr = stringify(query);
//     return httpClient(`${url}?${queryStr}`).then(({ headers, json }) => {
//       return {
//         data: json,
//         total: parseInt(headers.get('x-total-count'), 10),
//       };
//     });
//   },
//   // Implement other data provider methods (getOne, update, create, delete) based on your Express API's endpoints
// };

const AppBarLayout = (props) => <Layout {...props} appBar={CustomAppBar} />;

// CRUD Operations in Express app
    // Get all products
    // Get all active products
    // Create new product
    // Create single product
    // Update existing product
    // Archiving a product
    // Activating a product
    // User creation
    // Get user details


const App = () => (

    <Admin layout={AppBarLayout} dataProvider={dataProvider} >
        <Resource name="posts" options={{ label: 'Posts' }} list={PostList} show={PostShow} edit={PostEdit} icon={PostIcon} />
        <Resource name="users" options={{ label: 'Users' }} list={UserList} icon={UserIcon} />
        <Resource name="createuser" options={{ label: 'Create User' }} list={" "} />
        <Resource name="products" options={{ label: 'Product List' }} list={" "} />
        <Resource name="createproduct" options={{ label: 'Create Product' }} list={" "} />
        <Resource name="albums" options={{ label: 'Albums' }} list={ListGuesser} icon={AlbumIcon} />
    </Admin>

);

export default App;