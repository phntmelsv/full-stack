import { Datagrid, EmailField, List, TextField } from 'react-admin';
import { useState, useEffect, useContext } from 'react';


export const UserDetail = () => (
    <List>
        <Datagrid rowClick="edit">
            <TextField source="._id" />
            <TextField source="email" />
            <TextField source="password" />
            <TextField source="isAdmin" />
        </Datagrid>
    </List>
);

export default function getUserDetails() {
	function UserDetails(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
	}
}