import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){
	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>Learn from the best instructors</Card.Title>
			        <Card.Text>
			          All of our surf instructors are licensed.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>Affordable yet quality surf instruction</Card.Title>
			        <Card.Text>
			          As low as PHP 600, you can already start learning how to surf!
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>Flexible tutorials, depending on your skill level</Card.Title>
			        <Card.Text>
			          Whether you are a beginner, intermediate, or advance surfer, our surf school caters to various learners.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}
