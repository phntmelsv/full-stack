import { Fragment, useState, useContext } from 'react'
import {Container, Navbar, Nav} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import UserContext from '../UserContext'
import ReorderIcon from "@material-ui/icons/Reorder"
import '../styles/Navbar.css'

export default function AppNavbar(){

  const { user } = useContext(UserContext)


	return(
    <Navbar bg="light" expand="lg">
          <Container fluid>
            <Navbar.Brand as={Link} to="/">Lokal Surf Camp</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                { (user.id !== null) ?
                  // If the user has logged in, only show the logout link
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  :
                  // If the user has not logged in, show both login and register links
                  <Fragment>
                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                    <Nav.Link as={NavLink} to="/about">About</Nav.Link>
                  </Fragment>
                }
              </Nav>
            </Navbar.Collapse>
          </Container>
      </Navbar>
  )
}
