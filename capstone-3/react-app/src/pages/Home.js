import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import BannerImage from '../assets/vernon-raineil-cenzon-D7x38P_D9Ns-unsplash.jpg'
import '../styles/Home.css'

export default function Home(){
	return(
		<Fragment>
			<div className="headerContainer" style={{ backgroundImage:`url${BannerImage}`}}></div>
			<Banner/>
			<Highlights/>
		</Fragment>)
}