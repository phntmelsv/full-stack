import React from 'react'
import '../styles/Contact.css'

export default function Contact() {
	return (
		<div className="contact">
			<div className="leftSide"></div>
			<div className="rightSide"></div>
				<h1>Contact Us</h1>

				<h3>Don’t hesitate to reach out with the contact information below, or send a message using the form.</h3>
				</header>

				<form>
					<div class="form-group m-1">
						<label for="full_name">Name:</label>
						<input type="text" class="form-control" name="full_name" id="full_name" required autocomplete>
					</div>

					<div class="form-group m-1">
						<label for="email">Email address:</label>
						<input type="email" class="form-control" name="email" id="email" required autocomplete>
					</div>

					<div class="form-group m-1">
						<label for="notes">Message:</label>
						<!-- Creates a larger text input -->
						<textarea class="form-control" name="notes" rows="5" id="notes"></textarea>
					</div>

					<button type="submit" type="button" class="btn btn-dark mt-3 mb-3" data-toggle="modal" data-target="#exampleModal">Submit</button>
					<button type="reset" type="button" class="btn btn-dark mt-3 mb-3">Reset</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title text-dark" id="exampleModalLabel">Thank you for contacting us!</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body text-dark">
									Message Successfully Sent
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>

				</form>


		</div>
	)
}