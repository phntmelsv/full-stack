// Importables

import {Fragment, useState} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import About from './pages/About.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import ProductView from './components/ProductView.js';
import Footer from './components/Footer.js';
import {Container} from 'react-bootstrap';
import './App.css';
import { UserProvider } from './UserContext.js';



// Component function
// JSX = JavaScript + HTML (used by React); easier to implement JavaScript within HTML code
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () =>  {
    localStorage.clear()
  }

  // The component function returns JSX syntax that serves as the UI of the component
  // Note: JSX syntax may look like HTML but it is actually JavaScript that is formatted to look like HTML and is not actually HTML. 
  // <AppNavbar/> (self-closing) is the same as <AppNavbar></AppNavbar>
  return (
    // When rendering multiple components, they must always be enclosed in a parent component/element
    // <Route path="/register" element={<Register/>}/>
    // <Route path="/login" element={<Login/>}/>

    // The 'Routes' initialized the set of specific routes to be used
    // The 'Route' is the specific endpoint which will render a specific component

    <UserProvider value={{user, setUser, unsetUser}}>

      <Router> 
      {/* The 'Router' initializes that dynamic routing will be involved. */}
        <Container fluid>
            <AppNavbar/>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/products" element={<Products/>}/>
              <Route path="/products/:productId/view" element={<ProductView/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/about" element={<About/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<NotFound/>} />
            </Routes>
        </Container>
        <Footer />
      </Router>

    </UserProvider>


  );
}

// Exporting of the component function
export default App;
