// The 'document' represents the whole HTML page, and the query selector targets a specific element based on its ID, class, or tag name
console.log(document.querySelector("#txt-first-name"))

// These are the specific selectors you can use for targeting a specific target method (ID, class, tag name)
// Note: These aren't common to use anymore as compared to 'querySelector'
// document.getElementById("txt-first-name")
// document.getElementByClassName("span-full-name")
// document.getElementByTagName("input")

// [SECTION] Event Listeners
// The 'addEventListener' function listens for an event in a specific HTML tag. Antime an event is triggered, it will run the function in its 2nd argument
const txt_first_name = document.querySelector("#txt-first-name")
const txt_last_name = document.querySelector("#txt-last-name")

let span_full_name = document.querySelector(".span-full-name")


// Original 
/*
txt_first_name.addEventListener('keyup', (event) => {
	span_full_name.innerHTML = txt_first_name.value
})

*/

// Event handler function

function handleKeyUp() {
	// Retrieve the values from the input fields
	const firstName = txt_first_name.value
	const lastName = txt_last_name.value

	let fullName = firstName + " " + lastName
	span_full_name.innerHTML = fullName

}



// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself

// Original
/*
txt_last_name.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
})
*/

txt_first_name.addEventListener('keyup', (handleKeyUp) => {
	console.log(handleKeyUp.target)
	console.log(handleKeyUp.target.value)
});
txt_last_name.addEventListener('keyup', (handleKeyUp) => {
	console.log(handleKeyUp.target)
	console.log(handleKeyUp.target.value)
});


/*
Stretch goal:
1. Add 2 more elements for 'age' and 'address'
2. Listen to the two elements and display them in a new span tag along with the first name and last name 
3. The value of the new span tag must be in this format:
"Hello, I am <firstname> <lastname>, <age> years old. I am from <address>"

Optional: Add CSS to the form and put it in the center of the page with and H1 as the heading saying 'Sir Earl is handsome'

*/

