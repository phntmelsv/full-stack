import { useState, useEffect, useContext } from 'react'
import { Container, Card, Button, Row, Col } from 'react-bootstrap'
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function CourseView(){
	// Initializing the useNavigate
	const navigate = useNavigate()

	const {user} = useContext(UserContext)

	// Get the value courseId from the URL parameters (/courses/:courseId/view)
	const {courseId} = useParams()

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.message !== undefined){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: result.message
				})

				// Redirect to the Courses page after enrolling
				navigate("/courses")
			}
		}).catch(error => {
			Swal.fire({
				title: "Oopsie daisy",
				icon: "error",
				text: "Something went wrong"
			})
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(response => response.json())
		.then(result => {
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
		})
	}, [courseId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="my-3">
						<Card.Body>
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>

							<Card.Subtitle>Class Schedule:</Card.Subtitle>
							<Card.Text>8AM to 5PM</Card.Text>

							{ user.id !== null ?
								<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
								:
								<Link className="btn btn-warning" to="/login">Login to Enroll</Link>

							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}