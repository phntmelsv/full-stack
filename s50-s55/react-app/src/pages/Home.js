import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home(){
	return(
		<Fragment>
			<Banner/>
			<Highlights/>
			{/* MINI ACTIVITY - Create a 'CourseCard' component which will contain a  carrd from react-bootstrap and have the following properties:
				- Card Title = Sample Course
				- Card Subtitle = Description:
				- Card Text = This is a sample course
				- Card Subtitle = Price:
				- Card Text = Php 40,000
				- Button (primary) = Enroll

			After creating the component, add that component to the home page.
			*/}
		</Fragment>)
}