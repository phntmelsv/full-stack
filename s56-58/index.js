function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (letter.length !== 1) {
        // If letter is invalid, return undefined.
        return undefined;
    }

    let count = 0;
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            count++;
        }
    }

    return count;

}


function isIsogram(text) {
    // Convert the text to lowercase to disregard text casing
    text = text.toLowerCase();

    // Create an empty object to keep track of letter occurrences
    let letterCounts = {};

    // Iterate through each character in the text
    for (let i = 0; i < text.length; i++) {
        let letter = text[i];

        // Skip non-alphabetic characters
        if (!letter.match(/[a-z]/i)) {
            continue;
        }

        // Check if the letter has already occurred
        if (letterCounts[letter]) {
            return false; // Repeating letter found, return false
        }

        // Record the occurrence of the letter
        letterCounts[letter] = 1;
    }

    // No repeating letters found, return true
    return true;
}

function purchase(age, price) {
    // Check if age is below 13
    if (age < 13) {
        return undefined;
    }

    // Check if age is between 13 and 21 or 65 and above (student or senior citizen)
    if ((age >= 13 && age <= 21) || age >= 65) {
        // Calculate the discounted price
        let discountedPrice = (price * 0.8).toFixed(2);
        return String(discountedPrice);
    }

    // For ages 22 to 64
    let roundedPrice = price.toFixed(2);
    return String(roundedPrice);
}

function findHotCategories(items) {
    let categories = [];
    let hotCategories = [];

    // Iterate through each item in the items array
    for (let i = 0; i < items.length; i++) {
        let item = items[i];

        // Check if the category is already added to the categories array
        if (!categories.includes(item.category) && item.stocks === 0) {
            categories.push(item.category);
            hotCategories.push(item.category);
        }
    }

    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    let commonVoters = [];

    // Iterate through each voter in candidateA array
    for (let i = 0; i < candidateA.length; i++) {
        let voter = candidateA[i];

        // Check if the voter is also present in candidateB array
        if (candidateB.includes(voter)) {
            commonVoters.push(voter);
        }
    }

    return commonVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};